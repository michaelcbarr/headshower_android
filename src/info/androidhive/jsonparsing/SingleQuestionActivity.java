package info.androidhive.jsonparsing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import de.greenrobot.event.EventBus;
import model.Question;
import model.Response;

public class SingleQuestionActivity extends Activity {
	
	// JSON node keys
	private static final String TAG_NAME = "Name";
	private static final String TAG_TEXT = "Text";

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_question);


        //get data posted to message bus
        Question question = EventBus.getDefault().removeStickyEvent(Question.class);



//        // getting intent data
//        Intent in = getIntent();
//
//        // Get JSON values from previous intent
//        String qName = in.getStringExtra(TAG_NAME);
//        String qText = in.getStringExtra(TAG_TEXT);

        
        // Displaying all values on the screen
        TextView lblName = (TextView) findViewById(R.id.name_label);
        TextView lblText = (TextView) findViewById(R.id.text_label);

        RadioGroup responsesGroup = (RadioGroup) findViewById(R.id.responses_radio_group);

        lblName.setText(question.getName());
        lblText.setText(question.getText());

        //create radio button group
        List<Response> responses = question.getResponses();
        for (final Response response: responses) {
            RadioButton rb = new RadioButton(this);
            rb.setText(response.getHeader());
            //rb.setId(response.getValue());

            rb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("selectedResponse", response.getValue());
                    setResult(RESULT_OK,returnIntent);
                    finish();
                }
            });



            responsesGroup.addView(rb);

        }


    }


}
