package info.androidhive.jsonparsing;

import model.Condition;
import model.Question;
import model.Response;
import model.Result;
import model.iDiagnosable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.entity.StringEntity;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends ListActivity {

	private ProgressDialog pDialog;

	// URL to get contacts JSON
	private static String headshowerUrl = "http://headshowerservice.apphb.com/api/headshower/getCondition/";
    private static String diagnoseUrl = "http://headshowerservice.apphb.com/api/diagnose/post";
    //private static String diagnoseUrl = "http://192.168.1.133/HeadShowerService.Web/api/diagnose/post";

    private static String currentConditionName = "CatBehaviour";
    private static Result diagnosisResult = null;

    // JSON Node names
    private static final String TAG_NAME = "Name";
    private static final String TAG_TEXT = "Text";

	// list of questions
	List<Question> questions = null;

    // List of responses
    List<Response> responses = null;

	// Hashmap for ListView
	ArrayList<HashMap<String, String>> questionList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		questionList = new ArrayList<HashMap<String, String>>();

		final ListView lv = getListView();

		// Listview on item click listener
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
                //Try using EventBus to pass POJO to Activity
                Intent intent = new Intent(getApplicationContext(), SingleQuestionActivity.class);
                Question question = questions.get(position);
                de.greenrobot.event.EventBus.getDefault().postSticky(question);
                startActivityForResult(intent, question.getNumber());

			}
		});

        //Button click listener
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                scoreThisTest();
            }
        });

		// Calling async task to get json
		new GetCondition().execute();
	}

    private void scoreThisTest() {

        // Calling async task to get json
        new GetResult().execute();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            int selectedResponse=data.getIntExtra("selectedResponse", -1);


            //Check if this question has been answered
            if (responses != null)
            {
                int matchingIndex = -1;
                for (int i = 0; i < responses.size(); i++) {
                    int questionId = responses.get(i).getQuestion().getNumber();
                    if (questionId == requestCode)
                    {
                        matchingIndex = i;
                    }
                    break;
                }

                if (matchingIndex != -1)
                {
                    responses.remove(matchingIndex);
                }
            }


            Response response = new Response();
            response.setQuestion(questions.get(requestCode));
            response.setValue(questions.get(requestCode).getResponses().get(selectedResponse).getValue());

            responses.add(response);

//            Context context = getApplicationContext();
//            String responseText = questions.get(requestCode).getResponses().get(selectedResponse).getHeader();
//            CharSequence text = "selectedResponse = " + responseText;
//            int duration = Toast.LENGTH_SHORT;
//
//            Toast toast = Toast.makeText(context, text, duration);
//            toast.show();
        }

        else if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
    }//onActivityResult


    /**
	 * Async task class to get json by making HTTP call
	 * */
	private class GetCondition extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Loading questions...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			ServiceHandler sh = new ServiceHandler();

			// Making a request to url and getting response
			String jsonStr = sh.makeServiceCall(headshowerUrl + currentConditionName, ServiceHandler.GET);

			Log.d("Response: ", "> " + jsonStr);

			if (jsonStr != null) {
				try {

                    Gson gson = new Gson();
                    final Condition condition = gson.fromJson(jsonStr, Condition.class);


					// Getting JSON Array node
					questions = condition.getQuestions();

                    //Populate the responses object with correct number
//                    for (Question question : questions) {
//                        Response response = new Response();
//                        response.setQuestion(question);
//                        responses.add(response);
//                    }
                    responses = new ArrayList<Response>();


					// looping through All Questions
					for (int i = 0; i < questions.size(); i++) {
						Question q = questions.get(i);
						
						String name = q.getName();
						String text = q.getText();

						// tmp hashmap for single question
						HashMap<String, String> question = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						question.put(TAG_NAME, name);
                        question.put(TAG_TEXT, text);


						// adding question to question list
                        questionList.add(question);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
			/**
			 * Updating parsed JSON data into ListView
			 * */
			ListAdapter adapter = new SimpleAdapter(
					MainActivity.this, questionList,
					R.layout.list_item, new String[] { TAG_NAME, TAG_TEXT }, new int[] { R.id.name,
							R.id.text });

			setListAdapter(adapter);
		}

	}


    /**
     * Async task class to get Result of Qs making HTTP call
     * */
    private class GetResult extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Calculating Result...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            iDiagnosable iDiag = new iDiagnosable();
            iDiag.setScore(0);
            iDiag.setDiagnosis("");
            iDiag.setExplanation("");

            Gson gson = new Gson();

            com.google.gson.JsonObject json = new com.google.gson.JsonObject();
            StringEntity se = null;
            HashMap<String, Object> hashMap = new HashMap<String, Object>();



            try
            {
                // Here we say that the Hashmap takes Object as its value.
                hashMap.put("list",responses);
                hashMap.put("idiag", iDiag);
                hashMap.put("currentConditionName", currentConditionName);

            }
            catch (Exception e)
            {

            }


            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCallWithHashMap(diagnoseUrl, ServiceHandler.POST, hashMap);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {

                    diagnosisResult = gson.fromJson(jsonStr, Result.class);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
//            ListAdapter adapter = new SimpleAdapter(
//                    MainActivity.this, questionList,
//                    R.layout.list_item, new String[] { TAG_NAME, TAG_TEXT }, new int[] { R.id.name,
//                    R.id.text });
//
//            setListAdapter(adapter);

            CharSequence text = "Score = " + diagnosisResult.getScore() + "\nDiagnosis = " + diagnosisResult.getDiagnosis() + "\n Explanation = " + diagnosisResult.getExplanation();
            int duration = Toast.LENGTH_LONG;
            Context context = getApplicationContext();

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }

    }

}