package model;

import java.util.List;
import java.util.Vector;

import info.androidhive.jsonparsing.SingleQuestionActivity;

/**
 * Created by michaelbarr on 02/01/2014.
 */
public class Condition {

    private String Name;
    private String Description;
    private List<Question> Questions;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<Question> getQuestions() {
        return Questions;
    }

    public void setQuestions(List<Question> questions) {
        Questions = questions;
    }
}

