package model;

/**
 * Created by michaelbarr on 03/01/2014.
 */
public class iDiagnosable {

    public String getExplanation() {
        return Explanation;
    }

    public void setExplanation(String explanation) {
        Explanation = explanation;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public String getDiagnosis() {
        return Diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        Diagnosis = diagnosis;
    }

    private int Score;
    private String Diagnosis;
    private String Explanation;
}
