package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Vector;

/**
 * Created by michaelbarr on 02/01/2014.
 */
public class Question implements Parcelable {

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public int getSort() {
        return Sort;
    }

    public void setSort(int sort) {
        Sort = sort;
    }

    public QuestionType getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(QuestionType questionType) {
        QuestionType = questionType;
    }

    public List<Response> getResponses() {
        return Responses;
    }

    public void setResponses(List<Response> responses) {
        Responses = responses;
    }

    private String Name;
    private int Number;
    private String Text;
    private int Sort;

    private QuestionType QuestionType;
    private List<Response> Responses;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}



