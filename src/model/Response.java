package model;

/**
 * Created by michaelbarr on 02/01/2014.
 */
public class Response {

    private Question Question;

    public Question getQuestion() {
        return Question;
    }

    public void setQuestion(Question question) {
        Question = question;
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public int getOption() {
        return Option;
    }

    public void setOption(int option) {
        Option = option;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

    public int getSort() {
        return Sort;
    }

    public void setSort(int sort) {
        Sort = sort;
    }

    private String Header;
    private int Option;
    private int Value;
    private int Sort;
}


